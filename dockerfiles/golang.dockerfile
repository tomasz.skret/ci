FROM golang:alpine as builder

RUN  apk update \
  && apk add git curl \
  && apk add ca-certificates \
  && apk add tzdata zip

WORKDIR /usr/share/zoneinfo
RUN zip -r -0 /zoneinfo.zip .

RUN adduser --uid 10000 -D -g '' user

RUN mkdir -p /src/cmd

COPY go.mod /src
COPY go.sum /src
WORKDIR /src
RUN echo "Downloading dependencies" && go mod download

COPY . /src

ARG BUILD_DIR="."
ENV BUILD_DIR=${BUILD_DIR}

RUN echo "Building $BUILD_DIR" && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" -o /go/bin/app $BUILD_DIR

FROM scratch

ENV ZONEINFO /zoneinfo.zip
COPY --from=builder /zoneinfo.zip /
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd

COPY --from=builder /go/bin/app /app

USER user
ENTRYPOINT ["/app"]